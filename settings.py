#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random

def getWord():
    file = open("./wordList.txt")
    wordArray = file.readlines()
    for index, word in enumerate(wordArray):
        wordArray[index] = word.replace("\n", "")
    nb = random.randint(0, len(wordArray) - 1)
    return wordArray[nb]