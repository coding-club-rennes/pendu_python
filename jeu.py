#!/usr/bin/python3
# -*- coding: utf-8 -*-

from settings import getWord

# On récupère un mot à trouver
word = getWord()

# On affiche le mot
print(word)

# On affiche la taille du mot
print(len(word))